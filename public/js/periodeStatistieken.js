
$( document ).ready(function() {
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart
    $.ajax({
        method: 'POST',
        url: "/ajax/periodeChart",
        data: {_token: token}
    }).done(function (response) {
        response
        if(myChart != undefined)
        {
            myChart.destroy()
        }
        myChart = new Chart(ctx,response );
    })


    $('#weergeven').click(function () {
       var startDate  = $('#startDatum').val();
       var period = $('#periode').val();
        $.ajax({
            method: 'POST',
            url: "/ajax/periodeChart/" + startDate + "/" + period,
            data: {_token: token}
        }).done(function (response) {
            if(myChart != undefined)
            {
                myChart.destroy()
            }
            if(response['type'] == "Empty")
            {
                $('#emptyChartLabel').show()
                $('#emptyChartDiv').css("background-color", "rgba(220,220,220,0.5)")

                response['type'] = 'bar';

                myChart =  new Chart(ctx,response )
            }
            else
            {
                $('#emptyChartLabel').hide()
                $('#emptyChartDiv').css("background-color", "transparent")

                myChart =  new Chart(ctx,response );
            }

        })

    })



    console.log( "Check!" );

});
