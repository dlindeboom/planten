
$( document ).ready(function() {
    var ctx = document.getElementById('yearChart').getContext('2d');
    var myChart
    ctx
    $.ajax({
        method: 'POST',
        url: "/ajax/jaarChartTemperatuur",
        data: {_token: token}
    }).done(function (response) {
        var response = response
        myChart = new Chart(ctx,response );
    })


    $('#vergelijken').click(function () {
        var jaar1  = $('#jaar1').val();
        var jaar2 = $('#jaar2').val();


        var url = "/ajax/jaarChartTemperatuur";
        if(jaar1.length != 0)
        {
            url = url + "/" + jaar1
            if(jaar2.length != 0)
            {
                url = url + "/" + jaar2
            }
        }

        $.ajax({
            method: 'POST',
            url: url,
            data: {_token: token}
        }).done(function (response) {
            var response = response
            if(myChart != undefined)
            {
                myChart.destroy()
            }
            if(response['type'] == "Empty")
            {
                $('#emptyChartLabel').show()
                $('#emptyChartDiv').css("background-color", "rgba(220,220,220,0.5)")

                response['type'] = "bar";

                myChart =  new Chart(ctx,response )
            }
            else
            {
                $('#emptyChartLabel').hide()
                $('#emptyChartDiv').css("background-color", "transparent")

                myChart =  new Chart(ctx,response );
            }

        })

    })

    $('#bar,#line').click(function () {

        var jaar1  = $('#jaar1').val();
        var jaar2 = $('#jaar2').val();
        var chartType = $(this).attr('id')

        var url = "/ajax/jaarChartTemperatuur";
        if(jaar1.length != 0)
        {
            url = url + "/" + jaar1
            if(jaar2.length != 0)
            {
                url = url + "/" + jaar2
            }
        }

        $.ajax({
            method: 'POST',
            url: url,
            data: {_token: token}
        }).done(function (response) {
            if(myChart != undefined)
            {
                myChart.destroy()
            }
            if(response['type'] == "Empty")
            {
                $('#emptyChartLabel').show()
                $('#emptyChartDiv').css("background-color", "rgba(220,220,220,0.5)")

                response['type'] = chartType;

                myChart =  new Chart(ctx,response )
            }
            else
            {
                $('#emptyChartLabel').hide()
                $('#emptyChartDiv').css("background-color", "transparent")
                response['type'] = chartType;
                myChart =  new Chart(ctx,response );
            }

        })
    })


    console.log( "Check!" );

    console.log( "Check!" );

});
