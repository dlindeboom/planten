<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//get
// deze heb ik een naam geven zodat ik hier naar toe kan redirecten
Route::get('/', 'GrowthRegistrationController@index')->name('home');
Route::get('/overzicht', 'GrowthRegistrationController@show' );
Route::get('/statistieken/periode', 'Statistieken@periode');
Route::get('/statistieken/jaar', 'Statistieken@jaar');
Route::get('/statistieken/jaarTemperatuur', 'Statistieken@jaarTemperatuur');






//post
Route::post('groei/registreren', 'GrowthRegistrationController@store' );

//Ajax
Route::post('ajax/periodeChart/{startdate?}/{view?}', 'Statistieken@chartPeriod' );
Route::post('ajax/jaarChart/{year1?}/{year2?}', 'Statistieken@chartYear' );
Route::post('ajax/jaarChartTemperatuur/{year1?}/{year2?}', 'Statistieken@chartYearTemperature' );

//test
Route::get('/test/{year1?}/{year2?}', 'Statistieken@chartYear');
//Route::get('/test/2', 'Statistieken@chartYear');



