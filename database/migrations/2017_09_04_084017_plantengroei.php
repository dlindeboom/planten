<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Plantengroei extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GrowRegistration', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("total");
            $table->float("height");
            $table->float("temperature")->nullable();
            $table->date("regDate");
            $table->time("regTime");

        });

        Schema::create('Photos', function (Blueprint $table) {
            $table->increments('id');
            $table->text("photoName");
            $table->integer('planten_groei_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('GrowRegistration');
        Schema::dropIfExists('Photos');
    }
}
