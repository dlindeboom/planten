<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Generator as Faker;

class plantenGroeiSeed extends Seeder
{
    /**
     * Run the database seeds.
     * @var Faker instance of faker
     * @return void
     */
    public function run(Faker $faker)
    {
        //setting voor de seed
        $startYear = "1991";
        $endDate = Carbon::now()->format("Y-m-d");
        $maxDayDiff = 10;
        $minDayDiff = 5;
        $maxHeightDiff = 5;
        $mimHeightDiff = 2;
        $maxGrowthDiff = 1;
        $minGrowthDiff = 3;
        //Compare variable
        $carbon = Carbon::create($startYear,1,1);
        $startDate = $carbon->format("Y-m-d");
        $i = 1;


        while (strtotime($endDate) > strtotime($startDate)){

            $startDate = $carbon->addDays($faker->numberBetween($minDayDiff,$maxDayDiff));
            switch (intval($startDate->format('m')))
            {
                case 1:
                    $temperature = $faker->numberBetween(1, 7);
                    break;
                case 2:
                    $temperature = $faker->numberBetween(2, 7);
                    break;
                case 3:
                    $temperature = $faker->numberBetween(4, 8.8);
                    break;
                case 4:
                    $temperature = $faker->numberBetween(8.2, 13.1);
                    break;
                case 5:
                    $temperature = $faker->numberBetween(12.3, 16.0);
                    break;
                case 6:
                    $temperature = $faker->numberBetween(15.2, 18.8);
                    break;
                case 7:
                    $temperature = $faker->numberBetween(16.9, 35);
                    break;
                case 8:
                    $temperature = $faker->numberBetween(16.7, 35.2);
                    break;
                case 9:
                    $temperature = $faker->numberBetween(14.2, 20.5);
                    break;
                case 10:
                    $temperature = $faker->numberBetween(10.0,18.2);
                    break;
                case 11:
                    $temperature = $faker->numberBetween(5.5, 15.3);
                    break;
                case 12:
                    $temperature = $faker->numberBetween(2.7,10.7);
                    break;

            }



            if($startDate->format("z") < ($maxDayDiff * 2))
            {
                $height = $faker->numberBetween($mimHeightDiff, $maxHeightDiff) ;
                $growth = $faker->numberBetween($minGrowthDiff, $maxGrowthDiff);
            }
            elseif($startDate->format("z") > 90 && $startDate->format("z") < 100)
            {
                $growth = $faker->numberBetween($minGrowthDiff, $maxGrowthDiff);
                $height += $growth;
            }
            elseif($startDate->format("z") > 180 && $startDate->format("z") < 200)
            {
                $growth = $faker->numberBetween($minGrowthDiff, $maxGrowthDiff);
                $height += $growth;
            }
            elseif($startDate->format("z") > 300 && $startDate->format("z") < 320)
            {
                $growth = $faker->numberBetween($minGrowthDiff, $maxGrowthDiff);
                $height += $growth;
            }
            else
            {
                $height += $growth;
            }


            DB::table('GrowRegistration')->insert(
                [
                    'total' => $faker->numberBetween(1, 10),
                    'height' => $height,
                    'temperature' => $temperature,
                    'regDate' => $startDate,
                    'regTime' => $faker->time('His')
                ]);
            DB::table('Photos')->insert([
                'planten_groei_id' => $i++,
                'photoName' => 'storage/images/test' . $faker->numberBetween(1,4) . ".jpg"
            ]);
        }

    }
}
