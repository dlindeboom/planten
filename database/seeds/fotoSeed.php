<?php

use Illuminate\Database\Seeder;

class fotoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("fotos")->insert(
            [
                [
                    'fotoName' => 'images/test.jpeg',
                    'plantenGroei_id' => 1
                ],
                [
                    'fotoName' => 'images/test.jpeg',
                    'plantenGroei_id' => 2
                ],
                [
                    'fotoName' => 'images/test.jpeg',
                    'plantenGroei_id' => 3
                ],
                [
                    'fotoName' => 'images/test.jpeg',
                    'plantenGroei_id' => 4
                ],
                [
                    'fotoName' => 'images/test.jpeg',
                    'plantenGroei_id' => 5
                ],
                [
                    'fotoName' => 'images/test.jpeg',
                    'plantenGroei_id' => 6
                ],
                [
                    'fotoName' => 'images/test.jpeg',
                    'plantenGroei_id' => 7
                ],
                [
                    'fotoName' => 'images/test.jpeg',
                    'plantenGroei_id' => 8
                ],
                [
                    'fotoName' => 'images/test.jpeg',
                    'plantenGroei_id' => 9
                ],


        ]);
    }
}
