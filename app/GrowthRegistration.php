<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class GrowthRegistration extends Model
{
    // table die hij moet gebruiken van de database
    protected $table = "GrowRegistration";
    //aangezien ik niet met timestamps werk heb ik deze uit gezet
    public $timestamps = false;

    /**
     * Deze method haalt de foto op die gelinkt is aan deze registratie
     */
    public function Photo()
    {

        return $this->hasOne('App\Photo', 'planten_groei_id');
    }
    /**
     * @param $datumTijd Moet een geldig tijd en datum zijn
     * Deze method haalt de temperatuur op van de opgegeven tijd en slaat die op in de database
     */
    public function storeTemperature($datumTijd)
    {
        $carbon = new Carbon($datumTijd);
        //seconden naar 0 zetten
        $carbon->second = 0;
        //hier rond ik de tijd op het uur af zodat ik deze makelijk kan vergelijken later
        if($carbon->minute > 30)
        {
            $carbon->minute = 0;
        }
        else
        {
            $carbon->minute = 0;
            $carbon->hour++;
        }
        $timestamp = $carbon->timestamp;
        //Tempratuur data ophallen
        $json = file_get_contents("https://api.darksky.net/forecast/6f24a9e98542d19518c5c59adc9803f5/52.5167,6.083,$timestamp?units=si");
        //omzetten naar een json object
        $json = json_decode($json);


        foreach ($json->hourly->data as $hourlyData)
        {

            if($hourlyData->time == $timestamp)
            {
                $this->temperature = $hourlyData->temperature;
            }
        }

    }
    public function storeDateTime($datumtijd)
    {
        $carbon = new Carbon($datumtijd);
        $this->regDate = $carbon->format("Y-m-d");
        $this->regTime = $carbon->format("H:i:s");
    }


}
