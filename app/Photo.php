<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Photo extends Model
{

    //aangezien ik niet met timestamps werk heb ik deze uit gezet
    public $timestamps = false;
    protected $fillable = ['photoName'];
    protected $table = "Photos";
    /**
     * Deze method haalt de registratie op die gelinkt is aan deze foto
     */
    public function PlantenGroeien()
    {
       return $this->belongsTo('App\GrowthRegistration', 'planten_groei_id','ID');
    }
}
