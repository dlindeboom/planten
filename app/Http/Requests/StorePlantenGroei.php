<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class StorePlantenGroei extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $today = Carbon::now()->format("d-m-Y H:i");
        //validatie regels voor het registratieveld
        return [
            'datumTijd' => "date|required|before_or_equal:$today",
            'hoogte' => 'required|numeric|between:1,20000',
            'aantal' => 'required|numeric|between:1,20000',
            'Foto' => 'image|required'
        ];
    }
}
