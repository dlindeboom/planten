<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\GrowthRegistration;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use PhpParser\Node\Scalar\String_;

class Statistieken extends Controller
{
    /**
     * deze method zet de json response in elkaar voor charts.js functies
     *
     * @param $startdate Datum van waar de grafiek moet beginnen.
     * @param string $view  Opties:week,maand,jaar. als dit niet goed word ingegeven word die op een maand gezet
     * @return een Json response met daar in info voor chart.js om te gebruiken
     *
     *
     */
    public function chartPeriod($startdate = Null, $view = "maand")
    {
        //Start date omzetten naar een carbon Instance
        if($startdate == null)
        {
            $carbon = Carbon::now()->addWeeks(-1);
            $validDate = true;
        }
        else
        {
            //kijken of de startdate een geldig datum is
            if(strtotime($startdate) != false)
            {
                $carbon = new Carbon($startdate);
                $validDate = true;
            }
            else
            {
                $carbon = $carbon = Carbon::now()->addWeeks(5);
                $validDate = false;
            }

        }

        //tijd verschil bepalen
        if($view == "week")
        {
            $growthRegistrations= GrowthRegistration::whereBetween('regDate', [$carbon->format("Y-m-d"), $carbon->addWeek()->format('Y-m-d')]);
        }
        elseif ($view == "maand")
        {
            $growthRegistrations= GrowthRegistration::whereBetween('regDate', [$carbon->format("Y-m-d"), $carbon->addMonth()->format('Y-m-d')]);
        }
        elseif ($view == "jaar")
        {
            $growthRegistrations= GrowthRegistration::whereBetween('regDate', [$carbon->format("Y-m-d"), $carbon->addYear(1)->format('Y-m-d')]);

        }
        else
        {
            $growthRegistrations= GrowthRegistration::whereBetween('regDate', [$carbon->format("Y-m-d"), $carbon->addMonth()->format('Y-m-d')]);
        }


        $growthRegistrations =  $growthRegistrations->orderBy('regDate')->get();

        //controleren of er ook GrowthRegistrations zijn opgehaald
        if(count($growthRegistrations) && $validDate)
        {
            $labels = [];
            $dataset['hoogte'] = [];
            $dataset['temperatuur'] = [];
            foreach ($growthRegistrations as $growthRegistration)
            {
                $labels[] .=  date('d-m-Y', strtotime($growthRegistration->regDate));
                $dataset['hoogte'][] = $growthRegistration->height;
                $dataset['temperatuur'][] = $growthRegistration->temperature;
            }
            $returnVar['data']['labels'] = $labels;
            $returnVar['data']['datasets'][0]['data'] = $dataset['hoogte'] ;
            $returnVar['type'] = 'line';
            $returnVar['data']['datasets'][0]['label'] = "Hoogte";
            $returnVar['data']['datasets'][0]['backgroundColor'] = "rgba(153,255,51,0.6)";
        }
        else
        {
            //In javascript controller of dit dit iet de string "Empty" is. zowel krijgt de gebruiker een bericht dat
            //er geen data is opgehaald
            $returnVar['type'] = 'Empty';
        }
        $returnVar['options']['scales']['yAxes'][]['ticks']['beginAtZero'] = true;

        return response()->json(  $returnVar    );
    }

    /**
     * Deze methode haalt de gemiddelde temperatuur en hoogte op voor de geselecteerde jaren
     * @param string $year1 Moet een jaar zijn
     * @param string $year2 Moet een jaar zijn
     * @return een Json response met daar in info voor chart.js om te gebruiken
     */
    public function chartYearTemperature(String $year1  = Null,String $year2 = null)
    {


        if ($year1 == null)
        {
            $year1 = Carbon::now()->format('Y');
        }

        if ($year2 != null)
        {
            $growthRegistrations = GrowthRegistration::whereRaw("YEAR(regDate) = $year1 OR YEAR(regDate) = $year2");

        }
        else
        {
            $growthRegistrations = GrowthRegistration::whereYear('regDate', '=', $year1);
        }
        $growthRegistrations = $growthRegistrations->get();
        //Datums ophallen
        $months = $growthRegistrations->pluck('regDate');
        //Alleen de maanden ophallen
        $months = $months->map(function ($month){

            return date('m', strtotime($month));

        });
        //alleen de unique maanden verkrijgen
        $months = $months->unique()->toArray();
        sort($months);

        //collectie groeperen per jaar
        $growthRegistrations =  $growthRegistrations->groupBy(function ( $growthRegistration) {
            return date('Y', strtotime($growthRegistration->regDate));
        });
        //collectie groeperen per maand
        $growthRegistrations =  $growthRegistrations->map(function ( $growthRegistrationJaar) {
            return $growthRegistrationJaar->groupBy(function ($growthRegistrationMaand)
            {
                return date('m', strtotime($growthRegistrationMaand->regDate));
            });
        });

        //average hoogte ophallen
        $growthRegistrations =  $growthRegistrations->map(function ($growthRegistrationJaar)  {
            return  $growthRegistrationJaar->map(function ($growthRegistrationMaand) {
                    return [
                        number_format($growthRegistrationMaand->avg('height'),2,'.',','),
                        number_format($growthRegistrationMaand->avg('temperature'), 2, '.', ',')
                    ];
                }
            );
        }
        );

        //Instelling voor de chart.js functie
        $returnVar['type'] = 'line';
//        $returnVar['options']['scales']['yAxes'][]['stacked'] = true;
//        $returnVar['options']['scales']['xAxes'][]['stacked'] = true;
        $returnVar['options']['tooltips']['mode'] = 'index';
        $returnVar['data']['labels'] = $months;
        $growthRegistrations = $growthRegistrations->toArray();
        //Counter die voor de indexen van arrays wordt gebruikt
        $i = 0;

        if(count($growthRegistrations))
        {
            foreach ($growthRegistrations as $year => $growthRegistrationsMaand)
            {
                $labels = [];

                $dataset['hoogte'] = array_fill(1,12, 0);
                $dataset['temprature'] = array_fill(1,12, 0);
                foreach ($growthRegistrationsMaand as $month => $growthRegistration)
                {

                    $labels[] .=  date('d-m-Y', strtotime($growthRegistration[0]));
                    $dataset['hoogte'][intval($month)] = $growthRegistration[0];
                    $dataset['temprature'][intval($month)] = $growthRegistration[1];


                }
                $dataset['temprature'] = array_values($dataset['temprature']);
                $dataset['hoogte'] = array_values($dataset['hoogte']);

                if($i == 0)
                {
                    $returnVar['data']['datasets'][$i]['borderColor'] = "rgba(0,0,255,1)";
                    $returnVar['data']['datasets'][$i + 1]['borderColor'] = "rgba(255,255,0,1)";
                    $returnVar['data']['datasets'][$i]['backgroundColor'] = "rgba(0,0,255,1)";
                    $returnVar['data']['datasets'][$i + 1]['backgroundColor'] = "rgba(255,255,0,1)";
                }
                else
                {
                    $returnVar['data']['datasets'][$i]['borderColor'] = "rgba(0,255,255,1)";
                    $returnVar['data']['datasets'][$i + 1]['borderColor'] = "rgba(255,0,0,1)";
                    $returnVar['data']['datasets'][$i]['backgroundColor'] = "rgba(0,255,255,1)";
                    $returnVar['data']['datasets'][$i + 1]['backgroundColor'] = "rgba(255,0,0,1)";
                }

                $returnVar['data']['datasets'][$i]['fill'] = false;
                $returnVar['data']['datasets'][$i]['label'] = "Hoogte $year";
                $returnVar['data']['datasets'][$i]['data'] = $dataset['hoogte'] ;
                $returnVar['data']['datasets'][$i + 1]['label'] = "Temperatuur $year";
                $returnVar['data']['datasets'][$i + 1]['data'] = $dataset['temprature'] ;
                $returnVar['data']['datasets'][$i + 1]['fill'] = false;



                $i = $i + 2;



            }
        }
        else
        {
            //In javascript controller of dit dit iet de string "Empty" is. zowel krijgt de gebruiker een bericht dat
            //er geen data is opgehaald
            $returnVar['type'] = 'Empty';
        }



        return response()->json($returnVar)  ;

    }

    /**
     * Deze methode haalt de gemiddelde hoogte voor een range van jaren.
     *  @param string $year1 Moet een jaar zijn en het laagste jaar zijn
     * @param string $year2 Moet een jaar zijn en het hoogte jaar zijn
     * @return een Json response met daar in info voor chart.js om te gebruiken
     */
    public function chartYear(String $year1  = Null,String $year2 = null)
    {




        if ($year2 == null)
        {
            $year2 = Carbon::now()->format('Y');
        }
        if ($year1 == null )
        {
            $year1 = Carbon::createFromTimestamp(
                strtotime($year2))
                ->addYears(-10)
                ->format('Y');
        }

        if(is_numeric($year1) && is_numeric($year2)) {
            $growthRegistrations = GrowthRegistration::whereRaw("YEAR(regDate) BETWEEN $year1 AND  $year2")
                ->get()
                ->sortBy('regDate');;


            //Datums ophallen
            $months = $growthRegistrations->pluck('regDate');
            //Alleen de maanden ophallen
            $months = $months->map(function ($month) {

                return date('m', strtotime($month));

            });
            //alleen de unique maanden verkrijgen
            $months = $months->unique()->toArray();
            sort($months);

            //collectie groeperen per jaar
            $growthRegistrations = $growthRegistrations->groupBy(function ($growthRegistration) {
                return date('Y', strtotime($growthRegistration->regDate));
            });
            //collectie groeperen per maand
            $growthRegistrations = $growthRegistrations->map(function ($growthRegistrationJaar) {
                return $growthRegistrationJaar->groupBy(function ($growthRegistrationMaand) {
                    return date('m', strtotime($growthRegistrationMaand->regDate));
                });
            });
            //average hoogte ophallen
            $growthRegistrations = $growthRegistrations->map(function ($growthRegistrationJaar) {
                return $growthRegistrationJaar->map(function ($growthRegistrationMaand) {
                    return number_format($growthRegistrationMaand->avg('height'), 2, '.', ',');
                }
                );
            }
            );
            //Instelling voor de chart.js functie
            $returnVar['type'] = 'bar';
//        $returnVar['options']['scales']['yAxes'][]['stacked'] = true;
//        $returnVar['options']['scales']['xAxes'][]['stacked'] = true;
            $returnVar['options']['tooltips']['mode'] = 'index';
            $returnVar['data']['labels'] = $months;
            $growthRegistrations = $growthRegistrations->toArray();

            $i = 0;
        }
        else
        {
            $growthRegistrations  = Null;

        }
        if(count($growthRegistrations) != 0)
        {
            foreach ($growthRegistrations as $year => $growthRegistrationsMaand)
            {
                $labels = [];

                $dataset['hoogte'] = array_fill(1,12, 0);
                foreach ($growthRegistrationsMaand as $month => $growthRegistration)
                {

                    $labels[] .=  date('d-m-Y', strtotime($growthRegistration));
                    $dataset['hoogte'][intval($month)] = $growthRegistration;

                }

                $dataset['hoogte'] = array_values($dataset['hoogte']);

                $randomNumber = rand(0,255);
                $randomNumber2 = rand(0,255);
                $randomNumber3 = rand(0,255);
                $returnVar['data']['datasets'][$i]['label'] = "$year";
                $returnVar['data']['datasets'][$i]['backgroundColor'] = "rgba($randomNumber,$randomNumber2,$randomNumber3,0.5)";
                $returnVar['data']['datasets'][$i]['borderColor'] = "rgba($randomNumber,$randomNumber2,$randomNumber3,1)";
                $returnVar['data']['datasets'][$i]['data'] = $dataset['hoogte'] ;
                $returnVar['data']['datasets'][$i]['fill'] = false;
                $i++;

            }

        }
        else
        {
            $returnVar['type'] = "Empty";
        }


       return response()->json($returnVar)  ;
    }

    public function periode()
    {
        return view('statistieken.periode');
    }
    public function jaar()
    {
        $oldestYear = GrowthRegistration::oldest('regdate')->first();
        $oldestYear = date('Y', strtotime($oldestYear->regDate));
        return view('statistieken.jaar', compact('oldestYear'));
    }
    public function jaarTemperatuur()
    {
        $oldestYear = GrowthRegistration::oldest('regdate')->first()->pluck('regdate');
        return view('statistieken.temperatuur', compact('oldestYear'));
    }
}
