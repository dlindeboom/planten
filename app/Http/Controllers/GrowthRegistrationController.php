<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePlantenGroei;
use App\plantenGroei;
use App\GrowthRegistration;
use Carbon\Carbon;
use Faker\Generator as Faker;


class GrowthRegistrationController extends Controller
{
    public function index()
    {

        $currentTime = Carbon::now()->format('Y-m-d H:i');

        return view("index", compact('currentTime'));
    }

    /**
     * @param StorePlantenGroei $request
     * @return redirect naar de alias home
     */
    public function store(StorePlantenGroei $request)
    {

        try{
            //nieuwe instance van het model PlantenGroeien;
            $growthRegistration = new GrowthRegistration();
            // Velden in het nieuwe model vullen met de door gebruiker ingevulde velden
            $growthRegistration->total = $request->aantal;
            $growthRegistration->height = $request->hoogte;
            $growthRegistration->storeDateTime($request->datumTijd);
            $growthRegistration->storeTemperature($request->datumTijd);
            $growthRegistration->save();
            //foto linken aan de registratie
            $growthRegistration->Photo()->create([
                'photoName' => "storage/" .  substr($request->file('Foto')->store('public/images'),7)
            ]);
            $request->session()->flash('succes','Registratie was successful');
        }
        catch(\Exception $e){



            $request->session()->flash('failed',"Registratie is niet gelukt:");
        }


            return redirect()->home();
    }

    /**
     * Deze methode haalt de GrowthRegistration met de geselecteerde datum
     * @return $growthRegistrations it zijn alle GrowthRegistration's die zijn opgehaald met het geselecteerde datum
     * @return $selectedDate Dit is de geselecteerde datum
     */
    public function show()
    {
        $reqeust = request();
        $this->validate($reqeust,
            [
              'jaar' => "numeric|nullable|between:1970," . Carbon::now()->format("Y"),
              'maand'  => "numeric|nullable|between:1,12",
              'dag' => "numeric|nullable|between:1,31",
            ]
        );
        $year = $reqeust->jaar;
        $month = $reqeust->maand;
        $day = $reqeust->dag;
        $order = $reqeust->volgorde;
        //Dit variable wordt gebruikt als er geen resultaten terugkomen
        $selectedDate = [];

        $growthRegistrations = GrowthRegistration::orderBy('regDate',$order);

        if($day != null){
            $growthRegistrations->whereDay('regDate','=', $day);
            $selectedDate["Dag"] = $day;

        }

        if($month != null){
            $growthRegistrations->whereMonth('regDate','=', $month);
            $selectedDate["Maand"] = $month;
        }



        if($year == null && $day == null && $month == null){
            $year = Carbon::now()->format('Y');
            $growthRegistrations->whereYear("regDate", "=", $year);
            $selectedDate["Jaar"] = $year;
        }
        elseif($year != null)
        {
            $growthRegistrations->whereYear("regDate", "=", $year);
            $selectedDate["Jaar"] = $year;
        }

        $growthRegistrations = $growthRegistrations->get();




        return view('overzicht',compact('growthRegistrations', 'selectedDate'));
    }
}
