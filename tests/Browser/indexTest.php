<?php

namespace Tests\Browser;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class indexTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */

    public function testInputsIndex()
    {


                $this->browse(function ($browser)  {
            $browser->visit('/')
                ->type('datumTijd', "2017-09-07 16:34")
                ->type('hoogte', '10')
                ->type('aantal', 20)
                ->attach('Foto',  'C:\Users\Dennis Lindeboom\Downloads\test.jpg')
                ->press('button')
                ->assertSee('Registratie was successful');
        });
        $this->browse(function ($browser)  {
            $browser->visit('/')
                ->type('datumTijd', Carbon::now()->addDays(5)->format("Y-m-d H:i"))
                ->type('hoogte', '10')
                ->type('aantal', 20)
                ->attach('Foto',  'C:\Users\Dennis Lindeboom\Downloads\node-v6.11.2-x64.msi')
                ->press('button')
                ->assertSee('Oeps...');
        });
        $this->browse(function ($browser)  {
            $browser->visit('/')
                ->type('datumTijd', Carbon::now()->addDays(5)->format("Y-m-d H:i"))
                ->type('hoogte', 'as')
                ->type('aantal', 20e120)
                ->attach('Foto',  'C:\Users\Dennis Lindeboom\Downloads\node-v6.11.2-x64.msi')
                ->press('button')
                ->assertSee('Oeps...');
        });
        $this->browse(function ($browser)  {
            $browser->visit('/')
                ->type('datumTijd', Carbon::now()->addDays(-65)->format("Y-m-d H:i"))
                ->type('hoogte', '10')
                ->type('aantal', 20)
                ->attach('Foto',  'C:\Users\Dennis Lindeboom\Downloads\shadow.jpg')
                ->press('button')
                ->assertSee('Registratie was successful');
        });


    }

}
