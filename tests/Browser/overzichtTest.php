<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class overzichtTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */

    public function testInputOverzicht()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('http://plantengroei.dev/overzicht')->assertSee('2017');

        });

        $this->browse(function (Browser $browser) {
            $browser->visit('http://plantengroei.dev/overzicht')
                ->type('jaar', '2016')->press('button')->assertDontSee('2017');
        });
        $this->browse(function (Browser $browser) {
            $browser->visit('http://plantengroei.dev/overzicht')
                ->type('jaar', '2017')
                ->type('maand', '9')
                ->type('dag',31)
                ->press('button')
                ->assertDontSee('2016');
        });
        $this->browse(function (Browser $browser) {
            $browser->visit('http://plantengroei.dev/overzicht')
                ->type('jaar', '2014')
                ->type('maand', '9')
                ->type('dag',32)
                ->press('button')
                ->assertSee('Geen registraties voor:');
            $this->browse(function (Browser $browser) {
                $browser->visit('http://plantengroei.dev/overzicht')
                    ->type('jaar', 'aaaaaa')
                    ->type('maand', '9')
                    ->type('dag',31)
                    ->press('button')
                    ->assertSee('Geen registraties voor: ');
            });

            $this->browse(function (Browser $browser) {
                $browser->visit('http://plantengroei.dev/overzicht')
                    ->type('jaar', '2e3')
                    ->press('button')
                    ->assertSee('2000');
            });
            $this->browse(function (Browser $browser) {
                $browser->visit('http://plantengroei.dev/overzicht')
                    ->type('maand', '9')
                    ->press('button')
                    ->assertDontSee('Geen registraties voor:');
            });

        });
    }
}
