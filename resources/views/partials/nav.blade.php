<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">B.G.R.S</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="{{ Request::is('/') ? 'active' : '' }}">
                <a href="{{ url('/') }}">Groei registreren</a>
            </li>

            {{ Request::is('/overzicht') ? 'active' : '' }}
            <li class="{{ Request::is('overzicht') ? 'active' : '' }}">
                <a href="{{ url('/overzicht') }}">Overzicht</a>
            </li>
            <li class="dropdown  ">
                <a class="dropdown-toggle" data-toggle="dropdown">Statistieken
                    <span class="caret"></span></a>
                <ul class="dropdown-menu ">
                    <li class="{{ Request::is('statistieken/periode') ? 'active' : '' }}"><a href="{{ url('statistieken/periode') }}">Periode</a></li>

                    <li><a href="{{ url('statistieken/jaarTemperatuur') }} ">Temperatuur</a></li>
                    <li><a href="{{ url('statistieken/jaar') }} ">Jaar vergelijking</a></li>
                </ul>
            </li>

        </ul>
    </div>
    </nav>