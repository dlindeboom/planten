<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/general.css') }}" rel="stylesheet">
    <script src="{{asset('js/app.js')}}" ></script>
    <script src="{{asset('js/Chart.js')}}" ></script>
    <script src="{{asset('js/general.js')}}" ></script>

</head>