
@if( $flash =  session('succes'))

    <div  id="flash" style="position: absolute; z-index: 10; bottom: 0; right: 0" class="alert alert-success"> {{$flash}}</div>

    @endif

@if($flash =  session('failed'))

    <div  id='flash' style="position: absolute; z-index: 10; bottom: 0; right: 0" class="alert alert-danger"> {{$flash}}</div>
@endif
