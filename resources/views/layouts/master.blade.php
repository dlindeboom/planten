@include('partials.head')
@include('partials.nav')
@include('partials.flashMessages')

<div class="container">
    @if(count($errors) > 0)
    <!-- weergeeft alle errors van de validator-->
        @include('partials.errors')
    @endif

    @yield('content')
</div>
