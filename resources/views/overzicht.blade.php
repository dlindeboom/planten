
@extends('layouts.master')

@section('content')

    <form action="/overzicht" class="">

                <div class="row form-group">
                    <div  id="dayInput" class="col-sm-2 dateInput">
                        <input value="{{request('dag')}}" type="text" class="form-control" name="dag" id="dag" placeholder="DD">
                    </div>
                    <div  class="col-sm-2 dateInput">

                        <input  value="{{request('maand')}}" type="text" class="form-control" name="maand" id="maand" placeholder="MM">
                    </div>
                    <div  class="col-sm-2 dateInput">

                        <input value="{{request('jaar')}}" type="text" class="form-control " name="jaar" id="jaar" placeholder="YYYY">
                    </div  >



                    <div class="col-sm-3">
                        <select  value=""  required name="volgorde" class="form-control form-control-lg">
                            <option {{ (request('volgorde') == 'asc') ? 'selected' : '' }} value="asc">
                                Stijgend
                            </option>
                            <option  {{ (request('volgorde') == 'desc') ? 'selected' : '' }} value="desc">
                                Dalend
                            </option>
                        </select>
                    </div>
                </div>





        <div class="row  form-group">
            <div class="col-sm-12">
                <button class="btn col-sm-2" type="submit">Bekijken</button>
            </div>

        </div>





















    </form>
    @if(count($growthRegistrations) )
        @foreach($growthRegistrations->chunk(4) as $chunks)
            <div class="row">

                @foreach($chunks as $growRegistration)
                    <div class="wrapper col-md-3">
                        <div style=" overflow: hidden" class="RegistratieFoto">


                            <img style="" src="{{$growRegistration->Photo->photoName}}"> </div>
                        <div class="info">


                            <ul>
                                <li>Datum <strong>{{date('d-m-Y', strtotime($growRegistration->regDate))}}</strong></li>
                                <li>Tijd <strong>{{$growRegistration->regTime}}</strong></li>
                                <li>Hoogte: <strong>{{$growRegistration->height}} CM</strong> </li>
                                <li>Aantal: <strong>{{$growRegistration->total}}</strong></li>

                                <li>Temperatuur: <strong>{{round($growRegistration->temperature)}} C</strong> </li>
                            </ul>



                        </div>
                    </div>

                @endforeach
            </div>
        @endforeach
    @else
        <div class="alert alert-info">Geen registraties voor:
                @foreach($selectedDate as $key => $date)
                <br>{{$key}}
                    <strong>
                        {{$date}}
                    </strong>
                @endforeach
                 </div>
    @endif

    @endsection