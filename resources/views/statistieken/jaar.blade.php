
@extends('layouts.master')

@section('content')

            Van Jaar <input placeholder="{{$oldestYear}}" id="jaar1" name="jaar1">
        tot Jaar <input placeholder="{{\Carbon\Carbon::now()->format('Y')}}" id="jaar2" name="jaar2">
        <button type="button"  id="selecteren">Selecteren</button>

        <button id="bar" type="button">Bar</button><button  id="line" type="button">line</button>

    <div id="emptyChartDiv"> <canvas id="yearChart" width="750" height="400"></canvas>
    <label id="emptyChartLabel" style="display: none;    position: relative;
    /* margin: 0px auto; */
    top: 50%;
    text-align: center;
    left: 45%;">Geen data beschikbaar</label>



    <script > var token = '{{csrf_token()}}'

    </script>
    <script src="{{asset('js/myFunctions.js')}}"></script>
    <script src="{{asset('js/jaarStatistieken.js')}}"></script>
@endsection