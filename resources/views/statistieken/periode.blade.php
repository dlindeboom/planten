
@extends('layouts.master')

@section('content')


    Start datum: <input placeholder="dd-mm-yyyy" id="startDatum" name="startDatum">
    Periode weergave: <select id="periode">
        <option value="week">Week</option>
        <option value="maand">Maand</option>
        <option value="jaar">Year</option>
    </select>
    <button id="weergeven" type="button">Weergeven</button>


    <div id="emptyChartDiv"> <canvas id="myChart" width="750" height="400"></canvas>
        <label id="emptyChartLabel" style="display: none;    position: relative;
    /* margin: 0px auto; */
    top: 50%;
    text-align: center;
    left: 45%;">Geen data beschikbaar</label>




<script > var token = '{{csrf_token()}}'

</script>
<script src="{{asset('js/myFunctions.js')}}"></script>
<script src="{{asset('js/periodeStatistieken.js')}}"></script>
@endsection